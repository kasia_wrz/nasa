$(function () {
    
    
    //setting background for header
    function getBackground() {
        
        var header = $('header');
        
         $.ajax({
             url: 'https://api.nasa.gov/planetary/apod?api_key=SqrNjTE4BJ8omAC8w6VhqF0Xg499E2VU9smRKwNS&date=2017-02-09'
         })
         .done(function(response){
             header.css('background-image', 'url('+response.url+')');
         })
         .fail(function(err){
             console.log(err);
         })
         .always(function(){
             // ładowanie strony
         });
    }
    getBackground();
    
    //number of full pages with galleries
    var galNum = 1;
    
    //creating  a gallery in the container
    function gallery() {
        
        //creating 6 divs with pics
        for (var i = 0; i < 6; i++){
            
            var div = $('<div>').addClass('gallery-'+galNum).insertBefore($('.load'));  
        
            $.each(div, function(){
            
                var $th = $(this);
                 
                //getting random ID 
                var picID = Math.floor(Math.random()*(856-0+1)+0);
                
                var loading = $('#loading');
                loading.show();
                
                //getting the pic from API
                $.ajax({
                    url: 'https://api.nasa.gov/mars-photos/api/v1/rovers/curiosity/photos?sol=1000&page=2&api_key=SqrNjTE4BJ8omAC8w6VhqF0Xg499E2VU9smRKwNS'
                })
                 .done(function(response){
                    var picSrc = response.photos[picID].img_src;
                    $th.css('background-image', 'url('+picSrc+')');
                    console.log('success!');
                })
                .fail(function(err){
                    console.log(err);
                })
                .always(function(suc){
                    //loading page
                })
                loading.delay(18000).fadeOut(6000);
            });
        };

        //fn scrolling the page to the fist new div
       function scroll() {
            var pos = $('.gallery-'+galNum).first().offset().top;
            $('html, body').animate({scrollTop: pos}, 700);
        }
        scroll();  
        
        //preaparing class number for the next gallery
        galNum++;
        console.log(galNum);
    }
    gallery();
    
    //event getting new 6 pic
    $('.load').on('click', gallery);
    
});


/*
                //podstaw w fn
                //getting random day for the photo ID
                var year = Math.floor(Math.random()*(2016-2005+1)+2005),
                    month = Math.floor(Math.random()*(12-1+1)+1),
                    day =  Math.floor(Math.random()*(28-1+1)+1),
                    
                    */